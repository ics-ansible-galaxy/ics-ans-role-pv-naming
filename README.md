# ics-ans-role-pv-naming

Ansible role to install pv-naming.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-pv-naming
```

## License

BSD 2-clause
