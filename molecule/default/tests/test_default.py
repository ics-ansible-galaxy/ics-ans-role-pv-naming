import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_pv_naming(host):
    cmd = host.run("pv-naming --help")
    assert "Checking the meaning of a PV" in cmd.stdout
