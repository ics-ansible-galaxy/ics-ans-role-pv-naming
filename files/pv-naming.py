#!/usr/bin/env python3
# Implemented by Miklos Boros 2020
# ICS/HWI miklos.boros@ess.eu / borosmiklos@gmail.com
# v1.1 - Fixed Multiple DeviceName Queries
# v1.0 - First Release 2020.05.28

import urllib.request
import xml.etree.ElementTree as ET
import sys
import getopt
import argparse
from urllib.request import Request, urlopen
from urllib.error import URLError, HTTPError


namingurl = "https://naming.esss.lu.se"
deviceNameQuery = "/rest/deviceNames/search/"
partQuery = "/rest/parts/mnemonic/search/"

Name_description = ""
Name_deviceType = ""
Name_discipline = ""
Name_instanceIndex = ""
Name_name = ""
Name_system = ""
Name_status = ""
Name_subSystem = ""

Part_mnemonic = ""
Part_mnemonicPath = ""
Part_name = ""
Part_namePath = ""
Part_status = ""
Part_type = ""


def GetMnemonicSystemStructure(partName, mnemonicPath):
    global Part_mnemonic
    global Part_mnemonicPath
    global Part_name
    global Part_namePath
    global Part_status
    global Part_type
    request = urllib.request.Request(namingurl + partQuery + partName)
    response = urlopen(request).read()
    root = ET.fromstring(response)
    for subroot in root.findall("partElement"):
        foundMnemonic = subroot.find("mnemonic").text
        if foundMnemonic == partName:
            # Leading 'Acc-' characters are not useful here so trip them away.
            matchMnemonic = subroot.find("mnemonicPath").text
            if matchMnemonic[:4] == "Acc-":
                matchMnemonic = matchMnemonic[4:]
            if mnemonicPath == matchMnemonic:
                Part_mnemonic = foundMnemonic
                Part_mnemonicPath = subroot.find("mnemonicPath").text
                Part_name = subroot.find("name").text
                Part_namePath = subroot.find("namePath").text
                Part_status = subroot.find("status").text
                Part_type = subroot.find("type").text


def ProcessName(deviceName):
    global Name_description
    global Name_deviceType
    global Name_discipline
    global Name_instanceIndex
    global Name_name
    global Name_system
    global Name_status
    global Name_subSystem
    global Part_mnemonic
    global Part_mnemonicPath
    global Part_name
    global Part_namePath
    global Part_status
    global Part_type
    deviceNameURL = urllib.parse.quote_plus(deviceName)
    request = urllib.request.Request(namingurl + deviceNameQuery + deviceNameURL)
    response = urlopen(request).read()
    mainroot = ET.fromstring(response)
    foundit = 0
    for root in mainroot.findall("deviceNameElement"):
        Search_name = root.find("name").text
        if Search_name == deviceName:
            foundit += 1
            Name_discipline = root.find("discipline").text
            Name_instanceIndex = root.find("instanceIndex").text
            Name_name = root.find("name").text
            Name_system = root.find("system").text
            Name_status = root.find("status").text
            Name_subSystem = root.find("subsystem").text
            Name_deviceType = root.find("deviceType").text
            if Name_status == "ACTIVE":
                print("Your Device " + Name_name + " is ACTIVE. We are good to go.")
                if Name_name == deviceName:

                    # Get System
                    GetMnemonicSystemStructure(Name_system, Name_system)
                    if Part_type == "System Structure":
                        print(
                            "    System(System):       ["
                            + Part_mnemonic
                            + "] means: '"
                            + Part_name
                            + "' - "
                            + Part_status
                        )
                        SystemParent = Part_namePath.replace("-" + Part_name, "")
                    else:
                        print(
                            "Your System "
                            + Name_system
                            + " has an unexpected type: "
                            + Part_type
                        )

                    # Get SubSystem
                    GetMnemonicSystemStructure(
                        Name_subSystem, Name_system + "-" + Name_subSystem
                    )
                    if Part_type == "System Structure":
                        print(
                            "    SubSystem(SubSystem): ["
                            + Part_mnemonic
                            + "] means: '"
                            + Part_name
                            + "' - "
                            + Part_status
                        )
                        SubSystemParent = Part_namePath.replace(
                            "-" + Part_name, ""
                        ).replace(SystemParent + "-", "")
                    else:
                        print(
                            "Your SubSystem "
                            + Name_system
                            + " has an unexpected type: "
                            + Part_type
                        )

                    # Get Discipline
                    GetMnemonicSystemStructure(Name_discipline, Name_discipline)
                    if Part_type == "Device Structure":
                        print(
                            "    Discipline:            ["
                            + Part_mnemonic
                            + "] means: '"
                            + Part_name
                            + "' - "
                            + Part_status
                        )
                        DisciplineParent = Part_namePath.replace("-" + Part_name, "")
                    else:
                        print(
                            "Your Discipline "
                            + Name_system
                            + " has an unexpected type: "
                            + Part_type
                        )

                    # Get DeviceType
                    GetMnemonicSystemStructure(
                        Name_deviceType, Name_discipline + "-" + Name_deviceType
                    )
                    if Part_type == "Device Structure":
                        print(
                            "    DeviceType:            ["
                            + Part_mnemonic
                            + "] means: '"
                            + Part_name
                            + "' - "
                            + Part_status
                        )
                        DeviceTypeParent = Part_namePath.replace("-" + Part_name, "")
                    else:
                        print(
                            "Your DeviceType "
                            + Name_system
                            + " has an unexpected type: "
                            + Part_type
                        )
                    print("    InstanceIndex:         [" + Name_instanceIndex + "]")

                    print("    Naming Service Tree: " + SystemParent)
                    print("                          └── " + SubSystemParent)
                    print("                               └── " + DisciplineParent)
                    print("                                    └── " + DeviceTypeParent)
                    print(
                        "                                         └── "
                        + Name_deviceType
                    )

                else:
                    print(
                        "OK, this is strange. Your qeuery gave different name as expected. Abort."
                    )
            else:
                print(
                    "Your Device "
                    + Name_name
                    + " seems to be INACTIVE. Sorry, can't do more."
                )
    if foundit == 0:
        print(
            "We can't find your Device "
            + deviceName
            + " in the Naming Service. Note: names are case sensitive!"
        )


def main(argv):

    parser = argparse.ArgumentParser(
        description="Checking the meaning of a PV name in the Naming Service."
    )
    parser.add_argument(
        "PVs", metavar="PVName", type=str, nargs="+", help="ESS PV name to be checked"
    )

    args = parser.parse_args()

    req = Request(namingurl)
    try:
        response = urlopen(req, timeout=5)
    except HTTPError as e:
        print("The Naming Service couldn't fulfill the request.")
        print("Error code: ", e.code)
    except URLError as e:
        print("We failed to reach the Naming Service. " + namingurl)
        print("Reason: ", e.reason)
    else:
        print("Naming Service is working fine.")
        for arg in args.PVs:
            print("-" * 80)
            PV = arg.split(":", 2)[:2]
            ProcessName(":".join(PV))
    print("-" * 80)


if __name__ == "__main__":
    main(sys.argv[1:])
